all: draft-josefsson-openpgp-mailnews-header.txt draft-josefsson-openpgp-mailnews-header.html draft-josefsson-openpgp-mailnews-header-from--06.diff.html

draft-josefsson-openpgp-mailnews-header.txt: draft-josefsson-openpgp-mailnews-header.xml
	xml2rfc draft-josefsson-openpgp-mailnews-header.xml

draft-josefsson-openpgp-mailnews-header.html: draft-josefsson-openpgp-mailnews-header.xml
	xml2rfc draft-josefsson-openpgp-mailnews-header.xml draft-josefsson-openpgp-mailnews-header.html

draft-josefsson-openpgp-mailnews-header-from--06.diff.html: draft-josefsson-openpgp-mailnews-header-06.txt draft-josefsson-openpgp-mailnews-header.txt
	rfcdiff draft-josefsson-openpgp-mailnews-header-06.txt draft-josefsson-openpgp-mailnews-header.txt
